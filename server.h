#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QDataStream>
#include <QList>

struct Route{
    QString method, url, fileAddress;
};

class Server : public QObject
{
    Q_OBJECT
public:
    Server(QObject *pwgt);
    QTcpServer *tcpServer;
    bool startServer(int port);
    bool stopServer();
    void testConnection();
    void route(QString method, QString uri, QString fileAddress);

public slots:
    virtual void newConnection();
    void readClient();
    void gotDisconnection();

signals:
    void gotNewMesssage(QString msg);
    void smbDisconnected();

private:
    QList<QTcpSocket*> clients;
    QList<Route> routes;
};

#endif // SERVER_H
