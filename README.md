# Simple Linux Web Server

The server is made using QTcpServer and QTcpSockets. It can handle POST and GET requests showing html pages. The app has simple UI, which allows you to start/stop the server, test it(see the server status and the number of connections) and see the requests' body.

## Usage

To add a new route, use the function

```
  server->route("GET","/your/url","../pages/your_page.html");
```
POST request with url "/" returns the posted data.

## Examples

Request:
![Image1](images/1.png)

Result:
![Image2](images/2.png)



Request:
![Image3](images/3.png)

Result:
![Image4](images/4.png)



Request:
![Image5](images/5.png)

Result:
![Image6](images/6.png)
