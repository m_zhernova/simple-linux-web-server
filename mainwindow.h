#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define PORT 6547

#include <QDebug>
#include "server.h"
#include <QMainWindow>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_stopServer_clicked();
    void on_pushButton_startServer_clicked();
    void on_pushButton_testConn_clicked();
    void smbConnectedToServer();
    void smbDisconnectedFromServer();
    void gotNewMesssage(QString msg);

private:
    Ui::MainWindow *ui;
    Server *server;
};

#endif // MAINWINDOW_H
