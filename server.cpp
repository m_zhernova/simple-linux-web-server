#include "server.h"
#include <QDateTime>
#include <QFile>

Server::Server(QObject *pwgt) : QObject(pwgt)
{
    tcpServer = new QTcpServer(this);
}

bool Server::startServer(int port)
{
    if (!tcpServer->listen(QHostAddress::Any, port))
        return false;
    connect(tcpServer, &QTcpServer::newConnection, this, &Server::newConnection);
    return true;
}

bool Server::stopServer()
{
    if(tcpServer->isListening())
    {
        disconnect(tcpServer, &QTcpServer::newConnection, this, &Server::newConnection);
        tcpServer->close();
        return true;
    }
    else
        return false;
}

void Server::testConnection()
{
    if(tcpServer->isListening())
        emit gotNewMesssage( QString("%1 %2").arg("Server is listening, number of connected clients:").arg(QString::number(clients.count())));
    else
        emit gotNewMesssage( QString("%1 %2").arg("Server is not listening, number of connected clients:").arg(QString::number(clients.count())));
}

void Server::route(QString method, QString uri, QString fileAddress)
{
    routes.push_back(Route{method, uri, fileAddress});
}

void Server::newConnection()
{
    QTcpSocket *clientSocket = tcpServer->nextPendingConnection();
    connect(clientSocket, &QTcpSocket::disconnected, clientSocket, &QTcpSocket::deleteLater);
    connect(clientSocket, &QTcpSocket::readyRead, this, &Server::readClient);
    connect(clientSocket, &QTcpSocket::disconnected, this, &Server::gotDisconnection);
    clients << clientSocket;
}

void Server::readClient()
{
    QTcpSocket *clientSocket = (QTcpSocket*)sender();
    if(clientSocket->canReadLine()){
        char* buf = (char*)malloc(65535);
        clientSocket->read(buf,65535);
        QString line = buf;
        QStringList tokens = line.split(QRegExp("[ \r\n][ \r\n]*"));
        for(auto r : routes)
        {
            if(tokens[0]=="POST" && tokens[1] == "/")
            {
                QStringList tmp = line.split(QString("\r\n\r\n"));
                QString message = "HTTP/1.1 200 OK\r\n\r\n" + tmp[1];
                clientSocket->write(message.toStdString().c_str());
                clientSocket->close();
                emit gotNewMesssage( line );
                return;
            }
            if(tokens[0]==r.method && tokens[1] == r.url){
                QFile file(r.fileAddress);
                file.open(QIODevice::ReadOnly);
                QByteArray data=file.readAll();
                clientSocket->write("HTTP/1.1 200 OK\r\n\r\n" + data);
                clientSocket->close();
                emit gotNewMesssage( line );
                return;
            }
        }
        clientSocket->write("HTTP/1.1 200 OK\r\n\r\n Page not found!");
        clientSocket->close();
        emit gotNewMesssage( line );
    }
}

void Server::gotDisconnection()
{
    clients.removeAt(clients.indexOf((QTcpSocket*)sender()));
    emit smbDisconnected();
}
