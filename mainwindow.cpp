#include "mainwindow.h"
#include "ui_mainwindow.h"

QString cut(QString& line, QString delimiter)
{
    QString rez = line.split(delimiter).first();
    line.remove(0, rez.length() + delimiter.length());
    return rez;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    server = new Server(this);
    connect(server, &Server::gotNewMesssage, this, &MainWindow::gotNewMesssage);
    connect(server->tcpServer, &QTcpServer::newConnection, this, &MainWindow::smbConnectedToServer);
    connect(server, &Server::smbDisconnected, this, &MainWindow::smbDisconnectedFromServer);
}

MainWindow::~MainWindow()
{
    delete server;
    delete ui;
}

void MainWindow::on_pushButton_startServer_clicked()
{
    if (!server->startServer(PORT))
    {
        ui->textEdit_log->append(tr("<font color=\"red\"><b>Error!</b> The port is taken by some other service.</font>"));
        return;
    }
    QString msg = QString("http://127.0.0.1:") + QString::number(PORT);
    ui->textEdit_log->append(tr("<font color=\"green\"><b>Server started</b>, port is openned.</font>"));
    ui->textEdit_log->append(msg);
    server->route("GET","/wolf","../pages/example.html");
    server->route("GET","/","../pages/simple.html");
    QString req = QString("http://127.0.0.1:") + QString::number(PORT) + "/wolf";
    ui->textEdit_log->append("try GET " + req + " request");
    req = QString("http://127.0.0.1:") + QString::number(PORT) + "/";
    ui->textEdit_log->append("try GET " + req + " request");
    req = QString("http://127.0.0.1:") + QString::number(PORT) + "/";
    ui->textEdit_log->append("try POST " + req + " request: you will see the posted data");
}

void MainWindow::on_pushButton_stopServer_clicked()
{
    if(server->stopServer())
        ui->textEdit_log->append(tr("<b>Server stopped</b>, port is closed"));
    else
        ui->textEdit_log->append(tr("<b>Error!</b> Server was not running"));
}

void MainWindow::on_pushButton_testConn_clicked()
{
   server->testConnection();
}

void MainWindow::smbConnectedToServer()
{
    ui->textEdit_log->append(tr("Somebody has connected"));
}

void MainWindow::smbDisconnectedFromServer()
{
    ui->textEdit_log->append(tr("Somebody has disconnected"));
}

void MainWindow::gotNewMesssage(QString msg)
{
    QString  key;
    key = cut(msg, "\r\n");
    ui->textEdit_log->append(tr(QString("<font color=\"green\"> <b> %1 </b></font>").arg(key).toStdString().c_str()));
    while(true){
        key = cut(msg, "\r\n");
        ui->textEdit_log->append(QString("%1").arg(key));
        if(key=="")
            break;
    }
}

